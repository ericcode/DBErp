# DBErp

 **请到官方论坛下载最新版本 [https://bbs.loongdom.cn/forum.php?mod=forumdisplay&fid=76](https://bbs.loongdom.cn/forum.php?mod=forumdisplay&fid=76)** 

#### 介绍
DBErp 进销存系统，是北京珑大钜商科技有限公司 基于 Laminas + doctrine 2 开发的一套进销存系统。<br>

#### 本系统运行环境要求：

- 服务器系统：Linux（推荐）、Unix、Windows
- Web服务软件：Apache（推荐）、Nginx
- PHP版本：7.4及以上版本
- MySQL版本：5.6及以上版本


Web服务软件要求开启重写（Rewrite），使用Apache默认已经开启重写功能


#### PHP需要开启的扩展：
1. Curl
1. fileinfo
1. intl
1. openssl
1. PDO

#### DBErp系统安装过程：
 **最新版本已经自带在线安装功能，可下载最新版本进行安装 [https://bbs.loongdom.cn/forum.php?mod=forumdisplay&fid=76](https://bbs.loongdom.cn/forum.php?mod=forumdisplay&fid=76)** 

*从码云下载的程序在本地需要运行 [composer](https://getcomposer.org/) update，可以在 [https://bbs.loongdom.cn/forum.php?mod=forumdisplay&fid=76](https://bbs.loongdom.cn/forum.php?mod=forumdisplay&fid=76) 下载完整系统包*

#### 开源版使用须知
1. 可免费商业使用;
1. 免费商用必须保留版权信息，请自觉遵守;
1. 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负。

#### DBErp系统支持网址：

官方网站：[https://www.dberp.com.cn](https://www.dberp.com.cn)<br>
官方论坛：[https://bbs.loongdom.cn](https://bbs.loongdom.cn)<br>
演示DEMO：[http://demo.dberp.com.cn](http://demo.dberp.com.cn)<br>
演示账号 dberp
账号密码 123456


#### DBErp系统联系方式：
电子邮箱：support@dbshop.net<br>
QQ交流群：737830419

![QQ交流群扫码加入](https://loongdom-public.oss-cn-beijing.aliyuncs.com/git/20220627153244.png)